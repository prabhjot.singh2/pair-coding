import React, { useState } from "react";
import Container from "../../components/Container/Container";
import InputForm from "../../components/InputForm/InputForm";
import ResultList from "../../components/ResultList/ResultList";
import { getItem, setItem } from "../../utils/storage";

import "./FormContainer.css";

const FormContainer = () => {
  const savedData = JSON.parse(getItem("results")) || [];
  const [results, setResult] = useState(savedData);

  const handleSubmit = (url) => {
    fetch(`https://api.shrtco.de/v2/shorten?url=${url}`)
      .then((response) => {
        const resp = response.json();
        console.log(response);
        return resp;
      })
      .then((data) => {
        const res = {
          short_link: data.result.full_short_link2,
          link: data.result.original_link,
        };

        const savedResults = [...results, res];

        setResult(savedResults);
        setItem("results", JSON.stringify(savedResults));
      });
  };

  console.log(results);
  return (
    <Container customClass="input-container">
      <InputForm onSubmit={(e) => handleSubmit(e)} />
      <ResultList results={results} />
    </Container>
  );
};

export default FormContainer;
