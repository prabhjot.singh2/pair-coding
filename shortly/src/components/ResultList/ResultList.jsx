import React, { useState } from "react";
import Button from "../Button/Button";

import "./ResultLink.css";

const ResultList = ({ results }) => {
  const [copy, setCopy] = useState(false);

  const handleCopy = () => {
    setCopy(true);
  };

  const list = results.map((res, index) => (
    <div className="result-list" key={index}>
      <div>{res.link}</div>
      <div className="result-list-item">
        <div className="short-link">{res.short_link}</div>
        <Button
          isSquare
          title={copy ? "Copy" : "Copied!"}
          onClick={handleCopy}
          customClass="btn-clicked short-btn"
        />
      </div>
    </div>
  ));

  return <div>{list}</div>;
};

export default ResultList;
