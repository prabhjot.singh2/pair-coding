import React from "react";
import Anchor from "../Anchor/Anchor";

import "./FooterList.css";

const FooterList = ({ title, list }) => {
  const items = list.map((item, _) => (
    <li key={_} className="footer-links">
      <Anchor
        title={item.name}
        link={item.link}
        customClass="link footer-links"
        containerClass="footer-link-container"
      />
    </li>
  ));
  const listItem = <ul className="ui-item">{items}</ul>;

  return (
    <div className="footerList">
      <div className="footer-link-title">{title}</div>
      {list && <div>{listItem}</div>}
    </div>
  );
};

export default FooterList;
