import React from "react";

import Anchor from "../Anchor/Anchor";
import Container from "../Container/Container";
import FooterList from "../FooterList/FooterList";
import "./Footer.css";

const Footer = ({}) => {
  return (
    <Container customClass="full-width">
      <div className={`footer`}>
        <Anchor
          title="Shortly"
          customClass="logo-link white"
          containerClass="logo"
        />
        <div className="flex row">
          <FooterList
            title="Features"
            list={[
              { name: "Link Shortening", link: "#" },
              { name: "Branded Links", link: "#" },
              { name: "Analytics", link: "#" },
            ]}
          />
          <FooterList
            title="Resources"
            list={[
              { name: "Link Shortening", link: "#" },
              { name: "Branded Links", link: "#" },
              { name: "Analytics", link: "#" },
            ]}
          />
          <FooterList
            title="Company"
            list={[
              { name: "Link Shortening", link: "#" },
              { name: "Branded Links", link: "#" },
              { name: "Analytics", link: "#" },
            ]}
          />
        </div>
        <div>icons</div>
      </div>
    </Container>
  );
};

export default Footer;
