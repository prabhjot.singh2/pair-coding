import React from "react";

import "./Container.css";

const Container = ({ children, customClass = "" }) => {
  return <div className={`container ${customClass}`}>{children}</div>;
};

export default Container;
