import React from "react";

import "./Anchor.css";

const Anchor = ({
  title,
  link = "#",
  containerClass = "",
  customClass = "link",
}) => {
  return (
    <div className={`anchor ${containerClass}`}>
      <a href={link} className={customClass}>
        {title}
      </a>
    </div>
  );
};

export default Anchor;
