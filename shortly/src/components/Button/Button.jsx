import React from "react";

import "./Button.css";

const Button = ({ title, isRounded, isSquare, customClass = "", ...props }) => {
  return (
    <button
      {...props}
      className={`button ${isRounded ? "is-rounded" : ""} ${
        isSquare ? "is-square" : ""
      } ${customClass}`}
    >
      {title}
    </button>
  );
};

export default Button;
