import React, { useState } from "react";

import Button from "../Button/Button";

import "./InputForm.css";

const InputForm = ({ containerClass = "", customClass = "", onSubmit }) => {
  const [text, setText] = useState("");
  const handleChange = (e) => setText(e.target.value);

  const handleSubmit = (e) => {
    e.preventDefault();
    onSubmit(text);
  };
  return (
    <div className={`input ${containerClass}`}>
      <form className="form-div">
        <div className="input-box">
          <label>
            <input
              type="text"
              name="name"
              value={text}
              onChange={handleChange}
            />
          </label>
        </div>
        <div className="submit-btn">
          <Button
            isSquare
            customClass="submit-shorten"
            title="Shorten it!"
            onClick={handleSubmit}
          />
        </div>
      </form>
    </div>
  );
};

export default InputForm;
