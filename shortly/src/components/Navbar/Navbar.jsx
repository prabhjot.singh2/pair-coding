import React from "react";
import Anchor from "../Anchor/Anchor";
import Button from "../Button/Button";
import Container from "../Container/Container";

import "./Navbar.css";

const Navbar = () => {
  return (
    <Container>
      <div className="navbar">
        <div className="flex align-items-center">
          <Anchor title="Shortly" customClass="logo-link" />
          <Anchor title="Features" />
          <Anchor title="Pricing" />
          <Anchor title="Resources" />
        </div>

        <div className="flex align-items-center">
          <Button title="Login"></Button>
          <Button title="Sign Up" isRounded></Button>
        </div>
      </div>
    </Container>
  );
};

export default Navbar;
