
import Container from './components/Container/Container';
import Navbar from './components/Navbar/Navbar';

import './App.css';
import Footer from './components/Footer/Footer';
import Button from './components/Button/Button';
import FormContainer from './Containers/FormContainer/FormContainer';

function App() {
  return (
    <>
      <Navbar />
      <div className='section-2'>
        <Container>
          <div className='section-2-container' >
            <div className='section-2-text'>
              Lorem ipsum dolor sit amet
              <div className='section-2-sub-text'>
                Donec gravida nec ipsum semper laoreet. Donec blandit ornare congue.
                Nullam volutpat bibendum velit eu auctor. Sed nec leo urna.
              </div>
              <Button title='Get Started' customClass='section-button' isRounded />
            </div>
            <div className='section-2-image'>

            </div>
          </div>
        </Container>
      </div>

      <div className='section-4'>
        <Container>
          <FormContainer />
          <div className='section-container'>

            <div className='text-secondary text-center'>
              Advanced Statistics
            </div>
            <div className='text-tertiary text-center'>
              Track how your links are performing across the web with
              <br /> our advanced statistics dashboard
            </div>
          </div>
          <div className='section-card-container'>
            <div className='section-card'>
              <div className='card-title'>
                Brand recognition
              </div>
              <div className='card-description'>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Donec gravida nec ipsum semper laoreet. Donec blandit ornare congue.
                Nullam volutpat bibendum velit eu auctor. Sed nec leo urna.
              </div>
            </div>
            <div className='section-card section-card-2'>
              <div className='card-title'>
                Detailed Records
              </div>
              <div className='card-description'>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Donec gravida nec ipsum semper laoreet. Donec blandit ornare congue.
                Nullam volutpat bibendum velit eu auctor. Sed nec leo urna.
              </div>
            </div>
            <div className='section-card section-card-3'>
              <div className='card-title'>
                Fully Customizable
              </div>
              <div className='card-description'>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Donec gravida nec ipsum semper laoreet. Donec blandit ornare congue.
                Nullam volutpat bibendum velit eu auctor. Sed nec leo urna.
              </div>
            </div>
          </div>
        </Container>
      </div>
      <div className='section-3'>
        <Container>
          <div className='section-container' >
            <div className='text-primary text-center'>
              Boost your links today
            </div>
            <Button title='Get Started' customClass='section-button' isRounded />
          </div>
        </Container>
      </div>
      <Footer />
    </>
  );
}

export default App;
